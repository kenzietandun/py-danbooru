# py-danbooru

### python script to download imageboard galleries 

[![asciicast](https://asciinema.org/a/fA3ZTw3WxYckM0gz1q7LYXJcJ.png)](https://asciinema.org/a/fA3ZTw3WxYckM0gz1q7LYXJcJ?speed=2)

##### Supported sites

- [x] danbooru.donmai.us
- [x] chan.sankakucomplex.com
- [x] yande.re

##### Installation

- clone the repo `git clone https://gitlab.com/kenzietandun/py-danbooru.git`

- setup virtualenv `cd py-danbooru && virtualenv -p python3 ./ && source bin/activate && pip install -r req.txt`

##### Docker Image

- docker image is also available:

```
docker pull kenzietandun/danbooru
```

- save this as danbooru-docker.sh

```
#!/bin/bash

DOCKER_DIR='/docker/danbooru'

mkdir -p "$DOCKER_DIR"
cp py-danbooru.yml "$DOCKER_DIR"
touch "${DOCKER_DIR}/db.sqlite3"
mkdir -p "${DOCKER_DIR}/downloads"

docker stop danbooru
docker rm danbooru
docker rmi kenzietandun/danbooru
docker pull kenzietandun/danbooru
docker run -d \
  --name danbooru \
  -e PYTHONUNBUFFERED=0 \
  -v /docker/danbooru/downloads/:/app/downloads/ \
  -v /docker/danbooru/db.sqlite3:/app/db.sqlite3 \
  -v /docker/danbooru/py-danbooru.yml:/root/.config/py-danbooru.yml \
  kenzietandun/danbooru
```

- then run `./danbooru-docker.sh`

##### Usage

```
(danbooru) kenzie@vienna:~/git/danbooru/code$ ./danbooru.py -h
usage: danbooru.py [-h] [-y] [-s] [-p] [-w] [-m] [-yr] [-u URL]

imageboard downloader

optional arguments:
  -h, --help         show this help message and exit
  -y, --yandere      set imageboard to yande.re
  -s, --sankaku      set imageboard to sankakucomplex
  -p, --popular      download popular posts, defaults to daily from danbooru
  -w, --weekly       download weekly popular posts, defaults to danbooru
  -m, --monthly      download monthly popular posts, defaults to danbooru
  -yr, --yearly      download yearly popular posts, only in yande.re
  -u URL, --url URL  imageboard url
```

- Browse danbooru for tags that you want to archive, in this case I'm searching for `love_live!`

<img src='http://mirror.zies.net/pics/py-danbooru01.png' width='750'>

- run `danbooru.py` with search URL:

`./danbooru.py --url 'https://danbooru.donmai.us/posts?tags=love_live%21'`

- A file `dlfile.txt` will be created inside `downloads/love_live!` containing list of image links

- run `./dl.bash` to download the images

- Download weekly popular posts from sankakucomplex:

`./danbooru.py --sankaku --popular --weekly`

- Run without args to read from config file. Place your config file at `~/.config/py-danbooru.yml`

```
~$ cat ~/.config/py-danbooru.yml 
######### TEMPLATE ####
#---
#URL: [URL]
#     leave empty if popular: True
#rating: [sfw|nsfw]
#popular: [True]
#imageboard: [sankakucomplex|yandere|danbooru]
#            only needed if you set popular: True
#score: [integer > 0]
#time_range: [day|week|month|year]
#            only needed if you set popular: True
#            year is only for yandere imageboard 
#######################
---
URL: 
  - https://danbooru.donmai.us/posts?tags=senji_%28tegone_spike%29
  - https://danbooru.donmai.us/posts?page=1&tags=mignon
rating:
popular:
imageboard: 
score:
time_range:
---
URL: 
rating: nsfw
popular: True
imageboard: yandere
score:
time_range: day
```

##### Notes

- a sqlite db will be created to index images

- `danbooru.py` only generates list of links, to actually download the images, run `dl.bash`

- only tested for macOS and Linux

- patches are welcome
