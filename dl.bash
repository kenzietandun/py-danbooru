#!/usr/bin/env bash

cd downloads

for file in *
  do
    if [[ -d "$file" ]]
    then
      cd "$file"
      aria2c -i dlfile.txt -j 2 -c
      cd ..
    fi
  done
