FROM python:alpine3.6

WORKDIR /app/
COPY ./danbooru.py ./
COPY ./req.txt ./

RUN pip install --no-cache-dir -r req.txt

CMD ["python", "/app/danbooru.py"]
