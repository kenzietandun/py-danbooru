#!/usr/bin/env python3

from bs4 import BeautifulSoup as bs
from urllib.parse import urlsplit
from fake_useragent import UserAgent
from progress.bar import Bar
import sqlite3
import requests
import re
import sys
import os
import time
import threading
import shutil
import yaml

HOME = os.path.expanduser('~')
HERE = os.path.dirname(os.path.abspath(__file__))
ROOT_DANBOORU = 'https://danbooru.donmai.us/'
ROOT_SANKAKU = 'https://chan.sankakucomplex.com/'
ROOT_YANDERE = 'https://yande.re/'

# generate a random user agent for requests
# returns a header dictionary
def random_user_agent():
    ua = UserAgent()
    agent = ua.random
    headers = {'User-Agent': agent}
    return headers

RANDOM_USER = random_user_agent()

# get a full size image url from a post and writes it
# in dlfile.txt inside 'outputdir'
# takes url string and outputdir string as parameters
def get_image_url(url, outputdir, rating, score):
    if not os.path.isfile(outputdir + '/dlfile.txt'):
        os.system('touch {}'.format(outputdir + '/dlfile.txt'))

    base_url = "{0.scheme}://{0.netloc}/".format(urlsplit(url))
    EXTENSIONS = ('.png', '.jpg', '.jpeg', '.mp4', '.gif')

    score = int(score)

    try:
        page = requests.get(url, headers=RANDOM_USER).text
        soup = bs(page, "html.parser")
        if rating:
            if rating == 'sfw':
                if not 'Rating: Safe' in page:
                    return
            else:
                if 'Rating: Safe' in page:
                    return
    except Exception as e:
        print(e)
        return

    if 'chan.sankakucomplex.com' in base_url:
        try:
            if score > 0:
                post_score = re.match('.*Vote\sAverage\:\s(\d+(\.\d{1,2})?).*', page).group(1)
                if not post_score >= score:
                    return
            for litag in soup.find_all('li'):
                if 'Original:' in litag.text:
                    for link in litag.find_all('a'):
                        dl_link = 'https:' + link.get('href')
                        clean_dl_link = dl_link.split('?')[0]
                        with open(outputdir + '/dlfile.txt', 'r+') as f:
                            for line in f:
                                if clean_dl_link in line:
                                    break
                            else:
                                f.write(dl_link + '\n')
                        return
        except Exception as e:
            print('{} !!! [ERROR] {}'.format(e, url))
    elif 'yande.re' in base_url:
        try:
            if score > 0:
                for spantag in soup.find_all('span'):
                    #Score: <span id="post-score-410176">127</span>
                    if spantag.get('id') == 'post-score-{}'.format(url.split('/')[-1]):
                        post_score = spantag.text
                if not int(post_score) >= score:
                    return
            found_png = False
            found_jpg = False
            for litag in soup.find_all('li'):
                if 'Download larger version' in litag.text or 'Image (' in litag.text:
                    found_jpg = True
                    for link in litag.find_all('a'):
                        jpg_dl_link = link.get('href')
                if 'Download PNG' in litag.text:
                    found_png = True
                    for link in litag.find_all('a'):
                        png_dl_link = link.get('href')
            with open(outputdir + '/dlfile.txt', 'a') as f:
                if found_png:
                    f.write(png_dl_link + '\n')
                else:
                    f.write(jpg_dl_link + '\n')
        except Exception as e:
            print('{} !!! [ERROR] {}'.format(e, url))
    elif 'danbooru.donmai.us' in base_url:
        try:
            if score > 0:
                post_score = re.match('.*Score\:\s(\d+).*', page).group(1)
                if not post_score >= score:
                    return
            for link in soup.find_all('a'):
                hyp = link.get('href')
                if hyp and hyp.endswith(EXTENSIONS):
                    dl_link = base_url + hyp
                    with open(outputdir + '/dlfile.txt', 'a') as f:
                        f.write(dl_link + '\n')
                    return
        except Exception as e:
            print('{} !!! [ERROR] {}'.format(e, url))

# find image links in a page
# takes a soup object as a parameter
# returns a list of image URLs
def find_images_in_page(boorusite, page_soup):
    image_urls = []
    for link in page_soup.find_all('a'):
        hyperlink = link.get('href')
        if hyperlink:
            if boorusite == 'danbooru':
                if re.match('.*\/posts\/\d+', hyperlink):
                    image_urls.append(hyperlink)
            elif boorusite == 'sankakucomplex':
                if re.match('.*\/post\/show\/\d+', hyperlink):
                    image_urls.append(hyperlink)
            elif boorusite == 'yandere':
                if re.match('.*\/post\/show\/\d+', hyperlink):
                    image_urls.append(hyperlink)

    return image_urls

# handles requests for link
# takes url string as a parameter
def handle_request(url, tags, c):
    post_links = []
    print('Fetching page {}'.format(url))
    if re.match('https?\:\/\/danbooru\.donmai\.us.*', url):
        if '/explore/posts/popular' in url:
            # handle popular posts
            # 'popular' has only 1 page
            print('Popular page detected.')
            text = requests.get(url).text
            soup = bs(text, "html.parser")
            post_links = find_images_in_page('danbooru', soup)
            post_links = [ROOT_DANBOORU + link for link in post_links if re.match('.*\/posts\/\d+', link)]
        elif re.match('.*\/posts/\d+', url):
            # single image page
            print('Single page.')
            post_links.append(url)
        else:
            print('Gallery page.')
            for i in range(1, 1001):
                print('Fetching {} page {}'.format(url, i), end='\r')
                text = requests.get(url + '&page={}'.format(i)).text
                if 'Nobody here but us chickens!' in text:
                    break
                soup = bs(text, "html.parser")
                new_page = True
                new_links = find_images_in_page('danbooru', soup)
                for link in new_links:
                    postid = link.split('/')[-1]
                    new_page = check_db('danbooru', postid, tags, c)
                    if not new_page:
                        break
                post_links += new_links
                if not new_page:
                    break
            post_links = [ROOT_DANBOORU + link for link in post_links if not 'danbooru.donmai.us' in link]
    elif re.match('https?\:\/\/chan\.sankakucomplex\.com.*', url):
        if '/?tags=date%3A' in url:
            print('Popular page detected.')
            text = requests.get(url, headers=RANDOM_USER).text
            soup = bs(text, "html.parser")
            post_links = find_images_in_page('sankakucomplex', soup)
            post_links = [ROOT_SANKAKU + link for link in post_links]
        elif re.match('.*\/post\/show\/\d+', url):
            print('Single page.')
            post_links.append(url)
        else:
            # Sankaku only allows 25 pages
            print('Gallery page.')
            for i in range(1, 26):
                print('Fetching {} page {}'.format(url, i), end='\r')
                request_url = url + '&page={}'.format(i)
                text = requests.get(request_url, headers=RANDOM_USER).text
                if 'Nobody here but us chickens!' in text:
                    break
                soup = bs(text, "html.parser")
                new_page = True
                new_links = find_images_in_page('sankakucomplex', soup)
                for link in new_links:
                    postid = link.split('/')[-1]
                    new_page = check_db('sankakucomplex', postid, tags, c)
                    if not new_page:
                        break
                post_links += new_links
                if not new_page:
                    break
            post_links = [ROOT_SANKAKU + link for link in post_links if not 'chan.sankakucomplex.com' in link]
    elif re.match('https?\:\/\/yande\.re\/.*', url):
        if '/post/popular_recent' in url:
            print('Popular page detected.')
            text = requests.get(url).text
            soup = bs(text, "html.parser")
            post_links = find_images_in_page('yandere', soup)
            post_links = [ROOT_YANDERE + link for link in post_links if not 'yande.re' in link]
        elif re.match('.*\/post\/show\/\d+', url):
            print('Single page.')
            post_links.append(url)
        else:
            print('Gallery page.')
            max_page = get_max_page(url)
            for i in range(1, max_page + 1):
                print('Fetching {} page {}'.format(url, i), end='\r')
                request_url = url + '&page={}'.format(i)
                text = requests.get(request_url).text
                soup = bs(text, "html.parser")
                new_page = True
                new_links = find_images_in_page('yandere', soup)
                for link in new_links:
                    postid = link.split('/')[-1]
                    new_page = check_db('yandere', postid, tags, c)
                    if not new_page:
                        break
                post_links += new_links
                if not new_page:
                    break
            post_links = [ROOT_YANDERE + link for link in post_links if not 'yande.re' in link]
    else:
        print('Link not supported')
        sys.exit(1)
    return post_links

# get the max number of page a search allows
# takes a search page string url a parameter
# returns the max number of pages it allows you to download
def get_max_page(search_page):
    text = requests.get(search_page).text
    soup = bs(text, "html.parser")
    pages = []
    for page in soup.find_all('a'):
        link = page.get('href')
        if link and 'page=' in link:
            page_num = int(re.match('.*page=(\d+).*', link).group(1))
            pages.append(page_num)
    try:
        maxpage = max(pages)
    except:
        maxpage = 1
    print('Max page is {}'.format(maxpage))
    return maxpage

# checks if postid from imageboard is already in database
# takes imageboard name, postid, sqlite3 cursor, sqlite3 connection as parameters
# return True if the postid exists
def check_db(imageboard, postid, tags, cursor):
    cursor.execute("SELECT EXISTS(SELECT 1 FROM \"{imageboard}\" WHERE postid={postid} and tags=\"{tags}\"LIMIT 1)".format(
        imageboard=imageboard, postid=postid, tags=tags))
    record = cursor.fetchone()
    return record[0] == 0

# parse arguments passed to this script
# returns a list of task
#
# task is a dictionary containing informations
# to complete the task
def parse_arguments():
    tasks = []
    if len(sys.argv) == 1:
        print('Reading from config file')
        conf_path = HOME + '/.config/py-danbooru.yml'
        if not os.path.isfile(conf_path):
            print('{} not found'.format(conf_path))
            print('Creating one from template')
            sys.exit(1)
        conf_file = open(conf_path, 'r')
        confs = yaml.load_all(conf_file)
        for conf in confs:
            if type(conf['URL']) is list:
                for l in conf['URL']:
                    task = {'URL': l,
                            'rating': conf['rating'],
                            'time_range': conf['time_range'],
                            'score': conf['score'],
                            'popular': conf['popular'],
                            'imageboard': conf['imageboard']
                           }
                    tasks.append(task)
            else:
                task = {'URL': conf['URL'],
                        'rating': conf['rating'],
                        'time_range': conf['time_range'],
                        'score': conf['score'],
                        'popular': conf['popular'],
                        'imageboard': conf['imageboard']
                       }
                tasks.append(task)
        return tasks

    import argparse
    parser = argparse.ArgumentParser(description='imageboard downloader')
    parser.add_argument('-y', '--yandere', 
                        action='store_true', 
                        default=False,
                        dest='is_yandere',
                        help='set imageboard to yande.re'
                       )
    parser.add_argument('-s', '--sankaku', 
                        action='store_true', 
                        default=False,
                        dest='is_sankaku',
                        help='set imageboard to sankakucomplex'
                       )
    parser.add_argument('-p', '--popular', 
                        action='store_true', 
                        default=False,
                        dest='is_popular',
                        help='download popular posts, defaults to daily from danbooru'
                       )
    parser.add_argument('-w', '--weekly', 
                        action='store_true', 
                        default=False,
                        dest='is_weekly',
                        help='download weekly popular posts, defaults to danbooru'
                       )
    parser.add_argument('-m', '--monthly', 
                        action='store_true', 
                        default=False,
                        dest='is_monthly',
                        help='download monthly popular posts, defaults to danbooru'
                       )
    parser.add_argument('-yr', '--yearly', 
                        action='store_true', 
                        default=False,
                        dest='is_yearly',
                        help='download yearly popular posts, only in yande.re'
                       )
    parser.add_argument('-sfw', '--safe', 
                        action='store_true', 
                        default=False,
                        dest='is_sfw',
                        help='download only posts with safe rating'
                       )
    parser.add_argument('-nsfw', '--nsfw', 
                        action='store_true', 
                        default=False,
                        dest='is_nsfw',
                        help='download only posts with nsfw rating'
                       )
    parser.add_argument('-sc', '--score', 
                        dest='score',
                        help='only download if post is >= certain score, the score will be adjusted accordingly to each imageboards'
                       )
    parser.add_argument('-u', '--url', 
                        dest='URL',
                        help='imageboard url'
                       )

    args = parser.parse_args()
    URL = rating = popular = time_range = None

    if args.is_popular:
        popular = True

    if args.is_sfw:
        print('Applying SFW only.')
        rating = 'sfw'
    elif args.is_nsfw:
        print('Applying NSFW only.')
        rating = 'nsfw'

    score = 0
    if args.score:
        score = args.score

    if args.URL:
        URL = args.URL

    if args.is_popular:
        if args.is_yearly:
            time_range = 'year'
        elif args.is_monthly:
            time_range = 'month'
        elif args.is_weekly:
            time_range = 'week'
        else:
            time_range = 'day'

        if args.is_yandere:
            imageboard = 'yandere'
        elif args.is_sankaku:
            imageboard = 'sankakucomplex'
        else:
            imageboard = 'danbooru'
    
    try:
        if URL:
            base_url = "{0.scheme}://{0.netloc}/".format(
                urlsplit(URL)).split('/')[-2]
            print('Website is: {}'.format(base_url))
            imageboard = ''
            if base_url == 'chan.sankakucomplex.com':
                imageboard = 'sankakucomplex'
            elif base_url == 'danbooru.donmai.us':
                imageboard = 'danbooru'
            elif base_url == 'yande.re':
                imageboard = 'yandere'
    except:
        print('base_url not found')

    task = {'URL': URL,
            'rating': rating,
            'popular': popular,
            'time_range': time_range,
            'score': score,
            'imageboard': imageboard
           }

    tasks.append(task)
    return tasks

def get_popular_url(imageboard, time_range):
    URL = None
    import datetime
    today = datetime.datetime.now() - datetime.timedelta(1)
    if imageboard == 'sankakucomplex':
        if time_range == 'month':
            month_ago = (today - datetime.timedelta(days=30)).strftime('%Y-%m-%d')
            today = today.strftime('%Y-%m-%d')
            URL = 'https://chan.sankakucomplex.com/?tags=date%3A{month_ago}..{date}%20order%3Aquality'.format(date=today, month_ago=month_ago)
        elif time_range == 'week':
            week_ago = (today - datetime.timedelta(days=7)).strftime('%Y-%m-%d')
            today = today.strftime('%Y-%m-%d')
            URL = 'https://chan.sankakucomplex.com/?tags=date%3A{week_ago}..{date}%20order%3Aquality'.format(date=today, week_ago=week_ago)
        else:
            today = today.strftime('%Y-%m-%d')
            URL = 'https://chan.sankakucomplex.com/?tags=date%3A{date}%20order%3Aquality'.format(date=today)
    elif imageboard == 'yandere':
        if time_range == 'year':
            URL = 'https://yande.re/post/popular_recent?period=1y'
        elif time_range == 'month':
            URL = 'https://yande.re/post/popular_recent?period=1m'
        elif time_range == 'week':
            URL = 'https://yande.re/post/popular_recent?period=1w'
        else:
            URL = 'https://yande.re/post/popular_recent?period=1d'
    # is danbooru
    elif imageboard == 'danbooru':
        if time_range == 'month':
            URL = 'https://danbooru.donmai.us/explore/posts/popular?scale=month'
        elif time_range == 'week':
            URL = 'https://danbooru.donmai.us/explore/posts/popular?scale=week'
        else:
            URL = 'https://danbooru.donmai.us/explore/posts/popular'
    return URL

def process_task(task, conn, c):
    URL = task['URL']
    rating = task['rating']
    popular = task['popular']
    time_range = task['time_range']
    imageboard = task['imageboard']
    score = task['score']

    try:
        tags = re.match('.*tags\=(((\w+\-?(\w+)?)+)\+?((\-?\w+\%?\-?(\w+)?)+)?).*', URL).group(1)
        pattern = re.compile('[^\w_-]')
        tags = pattern.sub('_', tags)
        print('Detected tags: {}'.format(tags))
    except:
        tags = 'anon'

    if not score:
        score = 0

    outdir = ''
    if popular:
        print('Popular request detected')
        print('Using imageboard: {}'.format(imageboard))
        outdir = HERE + '/downloads/popular/'
        if not os.path.isdir(outdir):
            os.makedirs(outdir)
        URL = get_popular_url(imageboard, time_range)
        print('Using URL: {}'.format(URL))
    else:
        outdir = HERE + '/downloads/'
        if rating:
            outdir += '[' + rating.upper() + ']-'
        outdir += tags + '/'
        if not os.path.isdir(outdir):
            os.makedirs(outdir)
    print('Created dir {}'.format(outdir))

    if not imageboard:
        base_url = "{0.scheme}://{0.netloc}/".format(
            urlsplit(URL)).split('/')[-2]
        print('Website is: {}'.format(base_url))
        imageboard = ''
        if base_url == 'chan.sankakucomplex.com':
            imageboard = 'sankakucomplex'
        elif base_url == 'danbooru.donmai.us':
            imageboard = 'danbooru'
        elif base_url == 'yande.re':
            imageboard = 'yandere'

    post_links = handle_request(URL, tags, c)
    threads = []
    print('Found {} posts! Processing posts.'.format(len(post_links)))
    for link in post_links:
        postid = link.split('/')[-1]
        if postid.isdigit():
            new = check_db(imageboard, postid, tags, c)
            if new:
                # download
                threads.append((threading.Thread(target=get_image_url,
                                                args=(link, outdir, rating, score))
                              , 'INSERT OR IGNORE INTO {imageboard} VALUES({postid}, \"{tags}\")'.format(
                                  imageboard=imageboard, postid=postid, tags=tags)))

    bar = Bar('Processing images', max=len(threads))
    for t, cmd in threads:
        bar.next()
        t.start()
        c.execute(cmd)
        conn.commit()
        if imageboard == 'sankakucomplex':
            time.sleep(1.5)
        elif imageboard == 'danbooru':
            time.sleep(0.5)
        elif imageboard == 'yandere':
            time.sleep(1)
        else:
            time.sleep(0.5)
    bar.finish()
    for t, _ in threads:
        t.join()
    print('All done')
    print()

def connect_db():
    if not os.path.isfile(HERE + '/db.sqlite3'):
        print('Sqlite DB not found, creating one')
        os.system('touch {}'.format(HERE + '/db.sqlite3'))

    conn = sqlite3.connect(HERE + '/db.sqlite3')
    print('Connecting to Sqlite DB')
    c = conn.cursor()
    dbs = ['danbooru',
           'sankakucomplex',
           'yandere']
    for db in dbs:
        c.execute("CREATE TABLE IF NOT EXISTS {}(postid int, tags text);".format(db))
    conn.commit()
    return (conn, c)

def main():
    conn, c = connect_db()
    tasks = parse_arguments()
    for task in tasks:
        process_task(task, conn, c)

if __name__ == '__main__':
    main()
